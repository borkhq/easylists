// easyLists
// Written by KevY
// Contributors: Bork
// Current Version: v0.2b

// built-in include guard removal
// just in case the user has a local dependency with the same file name
#if defined _inc_easyLists
	#undef _inc_easyLists
#endif
// custom include-guard to ensure we don't duplicate
#if defined _easyLists_included
	#endinput
#endif
#define _easyLists_included

/* *********************************** 

				NATIVES

   *********************************** */

/*
	Name: List_New();
	Takes: Nothing
	Returns: List ID
	Usage: new id = List_New();

*/
native List_New();

/*
	Name: List_Add(listId, value);
	Takes: List ID and a value
	Returns: 0 (failed) or 1 (done)
	Usage: List_Add(playerList, playerid);

*/
native List_Add(listId, value);

/*
	Name: List_Remove(listId, value);
	Takes: List ID and a integer value (not index value)
	Returns: 0 (failed) or 1 (done)
	Usage: List_Remove(playerList, playerid);

*/
native List_Remove(listId, value);

/*
	Name: List_Size(listId);
	Alias: List_Count(listID);
	Takes: List ID
	Returns: INVALID_LIST_RESULT (-1) if failed or size of the list
	Usage: printf("Players online: %d", List_Size(playerList));

*/
native List_Size(listId);

/*
	Name: List_Contains(listId, value);
	Takes: List ID and a value to find
	Returns: INVALID_LIST_RESULT (-1) or 0 if failed, OR 1 if passed
	Usage: if(!List_Contains(playerList, playerid)) List_Add(playerList, playerid);

*/
native List_Contains(listId, value);

/*
	Name: List_Get(listId, index);
	Takes: List ID and the index to get the value of
	Returns: INVALID_LIST_INDEX or the value at the specified index
	Usage: printf("Value at index 3 of weapon list is %d", List_Get(weapons, 3));

*/
native List_Get(listId, index);

/* 
	Name: List_Find(listId, value);
	Takes: List ID and the value to find
	Returns: INVALID_LIST_RESULT (-1) or the index in the list where the value is found
	Usage: printf("Player ID 3 is on the index %d in the player list", List_Find(playerList, 3));

*/
native List_Find(listId, value);

/* 
	Name: List_Clear(listId);
	Takes: List ID
	Returns: INVALID_LIST_INDEX or 1 if executed (does not mean if it really cleared or not, just executed)
	Usage: List_Clear(Admins);

*/
native List_Clear(listId);


/* *********************************** 

				DEFINES

   *********************************** */

#define forlist(%1,%2) \
	for (new %1 = 0, __lstore_%1 = List_Size(%2); %1 < __lstore_%1; %1++)

#define INVALID_LIST_INDEX (65535)
#define INVALID_LIST_RESULT (-1)

#define List_Count List_Size

/* *********************************** 

				HOOKS

   *********************************** */

#define Player 0

native eLhook_OnPlayerConnect(playerid);
native eLhook_OnPlayerDisconnect(playerid);
native eLhook_OnGameModeExit();

public OnPlayerConnect(playerid)
{
    eLhook_OnPlayerConnect(playerid);

    #if defined eLHooks_OnPlayerConnect
        return eLHooks_OnPlayerConnect(playerid);
    #else
        return 1;
    #endif
}

#if defined _ALS_OnPlayerConnect
    #undef OnPlayerConnect
#else
    #define _ALS_OnPlayerConnect
#endif

#define OnPlayerConnect eLHooks_OnPlayerConnect

#if defined eLHooks_OnPlayerConnect
forward eLHooks_OnPlayerConnect(playerid);
#endif

/* ============================================= */

public OnPlayerDisconnect(playerid, reason)
{
    eLhook_OnPlayerDisconnect(playerid);

    #if defined eLHooks_OnPlayerDisconnect
        return eLHooks_OnPlayerDisconnect(playerid, reason);
    #else
        return 1;
    #endif
}

#if defined _ALS_OnPlayerDisconnect
    #undef OnPlayerDisconnect
#else
    #define _ALS_OnPlayerDisconnect
#endif

#define OnPlayerDisconnect eLHooks_OnPlayerDisconnect

#if defined eLHooks_OnPlayerDisconnect
forward eLHooks_OnPlayerDisconnect(playerid, reason);
#endif

/* ============================================= */

public OnGameModeExit()
{
    eLhook_OnGameModeExit();

    #if defined eLHooks_OnGameModeExit
        return eLHooks_OnGameModeExit();
    #else
        return 1;
    #endif
}

#if defined _ALS_OnGameModeExit
    #undef OnGameModeExit
#else
    #define _ALS_OnGameModeExit
#endif

#define OnGameModeExit eLHooks_OnGameModeExit

#if defined eLHooks_OnGameModeExit
forward eLHooks_OnGameModeExit();
#endif

/* ============================================= */